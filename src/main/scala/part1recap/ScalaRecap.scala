package part1recap

import java.util.concurrent.Executors
import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions
import scala.util.{Failure, Success, Try}

object ScalaRecap {

  //value
  val aBoolean: Boolean = false // in fact a constant
  var aVariable: Int = 56 // var can be changed later

  // expressions
  val anyInfExpression: String = if (2 > 3) "bigger" else "smaller"

  //instructions (executed) vs expressions (evaluated)
  val theUnit: Unit = println("Hello, Scala") //eq void

  //OOP

  class Animal

  class Cat extends Animal

  trait Carnivore {

    def eat(animal: Animal): Unit
  }

  // inheritance model: extends <= 1 class, but inherit from >= 0 traits

  class Crocodile extends Animal with Carnivore {
    override def eat(animal: Animal): Unit = println("eating this poor fellow")
  }

  //singleton
  object MySingleton

  //companions
  object Carnivore

  //case class
  case class Person(name: String, age: Int)

  //generics
  class MyList[A]

  // method notation
  // croc.eat(animal ) == croc eat animal

  val three = 1 + 2
  val three_v2 = 1.+(2)


  //FP
  val incrementor: Int => Int = x => x + 1
  val incremented = incrementor(4) // 5, same as incrementor.apply(4)


  //map, flatMap, filter

  val processedList = List(1, 2, 3).map(incrementor) // (2,3,4)

  val aLongerList = List(1, 2, 3).flatMap(x => List(x, x + 1)) // (1,2,2,3,3,4)

  // for comprehension

  val checkerboard = List(1, 2, 3).flatMap(n => List('a', 'b', 'c').map(c => (n, c)))
  val checkerboard_v2: Seq[(Int, Char)] = for {
    n <- List(1, 2, 3)
    c <- List('a', 'b', 'c')
  } yield (n, c) //same


  //options (presence/absence of values) and try (potentially failed computation)

  val anOption: Option[Int] = Option(/*something that migt be null*/ 43)
  val doubleOption = anOption.map(_ * 2) // _*2 == (x => x*2)

  val anAttempt: Try[Int] = Try(12)
  val modifiedAttempt = anAttempt.map(_ * 10)

  //pattern matching
  val anUnknown: Any = 45
  val medal: String = anUnknown match {
    case 1 => "gold"
    case 2 => "silver"
    case 3 => "bronze"
    case _ => "no medal"
  }

  //Futures

  val ec: ExecutionContext = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(8))

  implicit val aFuture = Future(1 + 999)(ec)

  aFuture.onComplete({
    case Success(value) => println(s"async meaning of life is $value")
    case Failure(exception) => println(s"the meaning og value failed $exception")
  })(ec)

  val aPartialFunction: PartialFunction[Try[Int], Unit] = {

    case Success(value) => println(s"async meaning of life is $value")
    case Failure(exception) => println(s"the meaning og value failed $exception")

  }

  //implicits

  // 1 - pass implicit arguments and values

  implicit val timeout: Int = 3000

  def setTimeout(f: () => Unit)(implicit tout: Int) = {
    Thread.sleep(tout)
    f()
  }

  setTimeout(() => println("timeout")) // (timeout)


  //2 - extension methods

  implicit class MyRichInt(number: Int) {
    def isEven: Boolean = number % 2 == 0
  }

  val is2Even: Boolean = 2.isEven //new RichInt(2).isEven


  // 3 - conversions

  implicit def string2Person(name: String): Person = Person(name, 21)


  val nikolay: Person = "Nikolay" // string2Person("Nikolay")


  def main(args: Array[String]): Unit = {

  }

}
