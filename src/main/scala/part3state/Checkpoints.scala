package part3state

import generators.shopping
import generators.shopping.{AddToShoppingCartEvent, ShoppingCartEvent, SingleShoppingCartEventsGenerator}
import org.apache.flink.api.common.functions.FlatMapFunction
import org.apache.flink.api.common.state.{CheckpointListener, ValueState, ValueStateDescriptor}
import org.apache.flink.runtime.state.{FunctionInitializationContext, FunctionSnapshotContext}
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.util.Collector

object Checkpoints {

  val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment


  //Set checkpoint intervals
  env.getCheckpointConfig.setCheckpointInterval(5000) //Checkpount triggered every 5s

  //Set checkpoint storage
  env.getCheckpointConfig.setCheckpointStorage("file:///D:/flink-essentials/checkpoints")

  /*
  Keep track of the number of AddToCart events PER USER, when quantity > a threshold (e.g. managing stock)

  Persist the data (state) via checkpoints
   */

  val shoppingCartEvents: DataStream[shopping.ShoppingCartEvent] = env.addSource(
    new SingleShoppingCartEventsGenerator(
      sleepMillisBetweenEvents = 100,
      generateRemoved = true
    )
  )


  //8> (Bob,1)
  //8> (Bob,2)
  //8> (Bob,3)
  //6> (Alice,1)
  //8> (Bob,4)
  //8> (Tom,1)
  //2> (Diana,1)
  //8> (Bob,5)
  //2> (Diana,2)
  //8> (Bob,6)
  //8> (Tom,2)
  //6> (Alice,2)
  //8> (Tom,3)
  //2> (Diana,3)
  //8> (Bob,7)
  //8> (Bob,8)
  //2> (Diana,4)
  //CHECKPOINT AT 1690375901066
  //CHECKPOINT AT 1690375901066
  //CHECKPOINT AT 1690375901066
  //CHECKPOINT AT 1690375901066
  //CHECKPOINT AT 1690375901066
  //CHECKPOINT AT 1690375901066
  //CHECKPOINT AT 1690375901066
  //CHECKPOINT AT 1690375901066
  val eventsByUser = shoppingCartEvents.keyBy(_.userId)
    .flatMap(new HighQuantityCheckpointedFunction(5))

  def main(args: Array[String]): Unit = {


    eventsByUser.print()

    env.execute()

  }


  class HighQuantityCheckpointedFunction(val threshold: Long)
    extends FlatMapFunction[ShoppingCartEvent, (String, Long)]
      with CheckpointedFunction
      with CheckpointListener {


    var stateCount: ValueState[Long] = _ //Instantiated Per Key

    override def flatMap(event: ShoppingCartEvent,
                         out: Collector[(String, Long)]): Unit = {

      event match {
        case AddToShoppingCartEvent(userId, sku, quantity, time) => {
          if (quantity > threshold) {

            //UPDATE STATE

            val newUserEventCount = stateCount.value() + 1
            stateCount.update(newUserEventCount)

            //PUSH OUTPUT
            out.collect(userId, newUserEventCount)

          }
        }
        case _ => //do nothing
      }

    }

    //Invoked when the checkpoint is TRIGGERED
    override def snapshotState(context: FunctionSnapshotContext): Unit = {
      println(s"CHECKPOINT AT ${context.getCheckpointTimestamp}")
    }

    //lifecycle method to initialize state (similar to open)
    override def initializeState(context: FunctionInitializationContext): Unit = {

      val stateCountDescriptor = new ValueStateDescriptor[Long]("impossibleOrderCount", classOf[Long])
      stateCount = context.getKeyedStateStore.getState(stateCountDescriptor)
    }

    override def notifyCheckpointComplete(checkpointId: Long): Unit = ()

    override def notifyCheckpointAborted(checkpointId: Long): Unit = ()
  }

}
