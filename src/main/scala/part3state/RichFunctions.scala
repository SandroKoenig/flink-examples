package part3state

import generators.shopping._
import org.apache.flink.api.common.functions.{MapFunction, RichMapFunction}
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.ProcessFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.util.Collector

object RichFunctions {

  val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

  env.setParallelism(1)

  val numbersStream: DataStream[Int] = env.fromElements(1, 2, 3, 4, 5, 6)

  //Pure functional programming
  val tenXNumbers: DataStream[Int] = numbersStream.map(_ * 10)


  // "explicit" map functions
  val tenXNumbers_v2: DataStream[Int] = numbersStream.map(new MapFunction[Int, Int] {
    override def map(value: Int): Int = {
      value * 10
    }
  })

  // Richmap functions
  val tenXNumbers_v3: DataStream[Int] = numbersStream.map(new RichMapFunction[Int, Int] {
    override def map(value: Int): Int = {
      value * 10
    }
  })

  // Richmap functions with lifecycle methods
  val tenXNumbers_v4: DataStream[Int] = numbersStream.map(new RichMapFunction[Int, Int] {

    //Mandatory Overwrite
    override def map(value: Int): Int = {
      value * 10
    }

    //Optional override
    // called BEFORE data goes through
    override def open(parameters: Configuration): Unit = {

      println("Starting my work!")
    }

    // called AFTER data goes through
    override def close(): Unit = {

      println("Finishing my Work")
    }
  })


  //PROCESS FUNCTION - Most general function abstraction
  val tenXNumbers_v5: DataStream[Int] = numbersStream.process(new ProcessFunction[Int, Int] {
    override def processElement(value: Int,
                                ctx: ProcessFunction[Int, Int]#Context,
                                out: Collector[Int]): Unit = {

      out.collect(value * 10)

    }

    override def open(parameters: Configuration): Unit = {
      println("Process Function starting")
    }

    override def close(): Unit = {
      println("Process Function finished")
    }

  })

  def main(args: Array[String]): Unit = {

    exercise()
  }


  /**
   * Exercise: "explode" all purches events to a single item
   * [(Boots,2),(iPhone,1)] ->
   * [(Boots),(Boots),(iPhone)]
   */

  def exercise(): Unit = {

    val exerciseEnv = StreamExecutionEnvironment.getExecutionEnvironment

    val shoppingCartStream: DataStream[AddToShoppingCartEvent] = exerciseEnv.addSource(new SingleShoppingCartEventsGenerator(1000)) //10events per second
      .filter(_.isInstanceOf[AddToShoppingCartEvent])
      .map(_.asInstanceOf[AddToShoppingCartEvent])


    val solutionWithProcessFunction = shoppingCartStream.process(new ProcessFunction[AddToShoppingCartEvent, String] {

      override def processElement(value: AddToShoppingCartEvent,
                                  ctx: ProcessFunction[AddToShoppingCartEvent, String]#Context,
                                  out: Collector[String]): Unit = {

        for (i <- 1 to value.quantity) out.collect(value.sku)
      }
    })


    solutionWithProcessFunction.print()
    exerciseEnv.execute()

  }


}
