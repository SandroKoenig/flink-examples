package part3state

import generators.shopping._
import org.apache.flink.api.common.state.MapStateDescriptor
import org.apache.flink.streaming.api.datastream.BroadcastStream
import org.apache.flink.streaming.api.functions.co.KeyedBroadcastProcessFunction
import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.util.Collector

object BoradcastState {

  val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

  val shoppingCartEvents: DataStream[ShoppingCartEvent] = env.addSource(new SingleShoppingCartEventsGenerator(100))

  val eventsByUser: KeyedStream[ShoppingCartEvent, String] = shoppingCartEvents.keyBy(_.userId)


  //issue a warning if quantity > threshold

  //  8> User Bob attempting to purchase 6 items of dbe9e514-04d3-4b6b-b753-4ed2dd32443d when threshold is 2
  //  1> User Sam attempting to purchase 6 items of 5f504c88-73dc-4dfa-ac60-9dd50ff945b4 when threshold is 2
  //  2> User Diana attempting to purchase 8 items of 2d74ec88-86a0-46d1-a531-639ee39edf2b when threshold is 2
  //  8> User Bob attempting to purchase 9 items of a7c3ba80-eba4-431e-aeb5-162f0102178f when threshold is 2
  def purchaseWarnings(): Unit = {

    val threshold = 2

    val notificationsStream = eventsByUser
      .filter(_.isInstanceOf[AddToShoppingCartEvent])
      .filter(_.asInstanceOf[AddToShoppingCartEvent].quantity > threshold)
      .map(event => event match {
        case AddToShoppingCartEvent(userId, sku, quantity, time) =>
          s"User $userId attempting to purchase $quantity items of $sku when threshold is $threshold"

        case _ => ""
      })

    notificationsStream.print()

  }


  //THRESHOLD CHANGES OVER TIME?
  // --> Thresholds will BROADCAST

  //2> User Diana attempting to purchase 4 items of 098c319c-14c3-47b9-8bb9-1c238861909e when threshold is 0
  //1> User Sam attempting to purchase 9 items of 3d2915da-fc90-4242-9bd0-2f2f4b15d197 when threshold is 0
  //8> User Bob attempting to purchase 8 items of 248efd92-eb5d-4e1a-9e8e-fa58b25613ae when threshold is 0
  //6> User Alice attempting to purchase 8 items of 6d440802-3060-4534-b57e-0a22a0f77ad0 when threshold is 0
  //8> User Tom attempting to purchase 8 items of eab202e1-b440-4bae-a6f3-fc4c0fecb981 when threshold is 0
  def changingThresholds(): Unit = {

    val thresholds: DataStream[Int] = env.addSource(new SourceFunction[Int] {

      override def run(ctx: SourceFunction.SourceContext[Int]): Unit = {

        List(2, 0, 4, 5, 6, 30).foreach(newThreshold => {
          Thread.sleep(1000)
          ctx.collect(newThreshold)
        })

      }

      override def cancel(): Unit = ()
    })

    val broadcastStateDescriptor = new MapStateDescriptor[String, Int]("thresholds", classOf[String], classOf[Int])

    val boradcastTresholds: BroadcastStream[Int] = thresholds.broadcast(broadcastStateDescriptor)

    val notificationsStream: DataStream[String] = eventsByUser
      .connect(boradcastTresholds) // KEY      FIRST EVENT   BROADCAST  OUTPUT
      .process(new KeyedBroadcastProcessFunction[String, ShoppingCartEvent, Int, String] {

        val thresholdDescriptor = new MapStateDescriptor[String, Int]("thresholds", classOf[String], classOf[Int])

        override def processElement(event: ShoppingCartEvent,
                                    ctx: KeyedBroadcastProcessFunction[String, ShoppingCartEvent, Int, String]#ReadOnlyContext,
                                    out: Collector[String]): Unit = {


          event match {
            case AddToShoppingCartEvent(userId, sku, quantity, time) =>
              val currentThreshold: Int = ctx.getBroadcastState(thresholdDescriptor).get("quantity-threshold")

              if (quantity > currentThreshold) {
                out.collect(s"User $userId attempting to purchase $quantity items of $sku when threshold is $currentThreshold")
              }
            case _ =>
          }


        }

        override def processBroadcastElement(newthreshold: Int,
                                             ctx: KeyedBroadcastProcessFunction[String, ShoppingCartEvent, Int, String]#Context,
                                             out: Collector[String]): Unit = {

          println(s"Threshold about to be changed -> $newthreshold")

          //fetch the broadcast state = distributed variable
          val statethresholds = ctx.getBroadcastState(thresholdDescriptor)

          //update the state
          statethresholds.put("quantity-threshold", newthreshold)

        }
      })


    notificationsStream.print()
  }

  def main(args: Array[String]): Unit = {

    changingThresholds()

    env.execute()

  }

}
