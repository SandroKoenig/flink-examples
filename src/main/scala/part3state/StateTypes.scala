package part3state

import generators.shopping._
import org.apache.flink.api.common.state.{ListState, ListStateDescriptor, MapState, MapStateDescriptor, StateTtlConfig, ValueState, ValueStateDescriptor}
import org.apache.flink.api.common.time.Time
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.KeyedProcessFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.util.Collector

import scala.collection.JavaConverters._ //Implicit converters

object StateTypes {

  val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

  val shoppingCartEvents: DataStream[ShoppingCartEvent] = env.addSource(
    new SingleShoppingCartEventsGenerator(
      sleepMillisBetweenEvents = 100, //10Events per sec
      generateRemoved = true,
    )
  )


  def demoValueState(): Unit = {

    /*
    How many events PER USER have been generated?
     */
    val eventsPerUser: KeyedStream[ShoppingCartEvent, String] = shoppingCartEvents.keyBy(_.userId)

    val numberOfEventsPerUserNaive = eventsPerUser.process(
      //                         KEY       EVENT             RES
      new KeyedProcessFunction[String, ShoppingCartEvent, String] { //instantiated once per operator task

        var nEventsPerTask = 0;

        override def processElement(value: ShoppingCartEvent,
                                    ctx: KeyedProcessFunction[String, ShoppingCartEvent, String]#Context,
                                    out: Collector[String]): Unit = {

          nEventsPerTask += 1

          out.collect(s"User ${value.userId} - ${nEventsPerTask}")

        }
      }
    )


    /*
    Problems with local vars:
      - they are local, so not accessible for others
      - if a node crashes, the var = state is lost
     */

    val numberOfEventsPerUserStream = eventsPerUser.process(

      new KeyedProcessFunction[String, ShoppingCartEvent, String] {

        //can call .value to get current state
        //can call .update(newValue) to overwrite
        var stateCounter: ValueState[Long] = _

        override def open(parameters: Configuration): Unit = {

          //initilaize all the state

          stateCounter = getRuntimeContext //From RichFunction
            .getState(new ValueStateDescriptor[Long]("events-counter", classOf[Long]))

        }

        override def processElement(value: ShoppingCartEvent,
                                    ctx: KeyedProcessFunction[String, ShoppingCartEvent, String]#Context,
                                    out: Collector[String]): Unit = {

          val nEventsForThisUser = stateCounter.value()
          stateCounter.update(nEventsForThisUser + 1)

          out.collect(s"User ${value.userId} - ${nEventsForThisUser}")

        }
      }


    )

    numberOfEventsPerUserStream.print()
    env.execute()

  }

  //ListState

  //  User Bob - [AddToShoppingCartEvent(Bob,649683cc-2eb7-4ec1-9fef-b08d19292e6f,8,2023-07-26T11:35:33.227600700Z)]
  //  6> User Alice - [RemovedFromShoppingCartEvent(Alice,a1300e57-797d-4d72-a8b1-5e9e9776d4e8,3,2023-07-26T11:35:33.227600700Z)]
  //  8> User Tom - [AddToShoppingCartEvent(Tom,c0471eda-860f-4bd5-b454-d64c495fa748,1,2023-07-26T11:35:33.227600700Z)]
  //  8> User Tom - [AddToShoppingCartEvent(Tom,c0471eda-860f-4bd5-b454-d64c495fa748,1,2023-07-26T11:35:33.227600700Z),AddToShoppingCartEvent(Tom,6926c3a2-6bfc-4b81-891a-e02b90db7be0,0,2023-07-26T11:35:33.227600700Z)]
  //  8> User Tom - [AddToShoppingCartEvent(Tom,c0471eda-860f-4bd5-b454-d64c495fa748,1,2023-07-26T11:35:33.227600700Z),AddToShoppingCartEvent(Tom,6926c3a2-6bfc-4b81-891a-e02b90db7be0,0,2023-07-26T11:35:33.227600700Z),RemovedFromShoppingCartEvent(Tom,e8cabc96-e52e-420c-834b-740c89ed1938,7,2023-07-26T11:35:33.227600700Z)]
  //  8> User Bob - [AddToShoppingCartEvent(Bob,649683cc-2eb7-4ec1-9fef-b08d19292e6f,8,2023-07-26T11:35:33.227600700Z),RemovedFromShoppingCartEvent(Bob,c2a2f06e-2988-45d8-a705-da2431752f47,8,2023-07-26T11:35:33.227600700Z)]

  def demoListState(): Unit = {

    val eventsPerUser: KeyedStream[ShoppingCartEvent, String] = shoppingCartEvents.keyBy(_.userId)

    //store all the events by user id
    val allEventsPerUserStream = eventsPerUser.process(
      new KeyedProcessFunction[String, ShoppingCartEvent, String] {

        //Need to be careful to keep the list bounded!
        var stateEventsForuser: ListState[ShoppingCartEvent] = _

        override def open(parameters: Configuration): Unit = {
          stateEventsForuser = getRuntimeContext.getListState(
            new ListStateDescriptor[ShoppingCartEvent]("shopping-cart-events", classOf[ShoppingCartEvent])
          )
        }

        override def processElement(value: ShoppingCartEvent,
                                    ctx: KeyedProcessFunction[String, ShoppingCartEvent, String]#Context,
                                    out: Collector[String]): Unit = {

          stateEventsForuser.add(value)

          val currentEvents: Iterable[ShoppingCartEvent] = stateEventsForuser.get() // returns a java iterable
            .asScala //convert to scala iterable

          out.collect(s"User ${value.userId} - [${currentEvents.mkString(",")}]")

        }
      }
    )

    allEventsPerUserStream.print()
    env.execute()

  }


  //  1> Sam - AddToShoppingCartEvent=1
  //  6> Alice - AddToShoppingCartEvent=1
  //  8> Bob - AddToShoppingCartEvent=1
  //  2> Diana - RemovedFromShoppingCartEvent=1
  //  1> Sam - AddToShoppingCartEvent=2
  //  1> Sam - AddToShoppingCartEvent=3
  //  6> Alice - RemovedFromShoppingCartEvent=1,AddToShoppingCartEvent=1
  //  6> Alice - RemovedFromShoppingCartEvent=2,AddToShoppingCartEvent=1
  //  2> Diana - RemovedFromShoppingCartEvent=2

  def demoMapState(): Unit = {

    //Count how many events PER TYPE were ingested PER USER

    val eventsPerUser: KeyedStream[ShoppingCartEvent, String] = shoppingCartEvents.keyBy(_.userId)


    val countsPerType = eventsPerUser.process(
      new KeyedProcessFunction[String, ShoppingCartEvent, String] {

        var stateCountsPerEventType: MapState[String, Int] = _

        override def open(parameters: Configuration): Unit = {

          stateCountsPerEventType = getRuntimeContext.getMapState(
            new MapStateDescriptor[String, Int]("per-type-counter", classOf[String], classOf[Int])
          )

        }

        override def processElement(value: ShoppingCartEvent,
                                    ctx: KeyedProcessFunction[String, ShoppingCartEvent, String]#Context,
                                    out: Collector[String]): Unit = {


          //fetch type of the event
          val eventType = value.getClass.getSimpleName

          if (stateCountsPerEventType.contains(eventType)) {

            val oldCount = stateCountsPerEventType.get(eventType)
            val newCount = oldCount + 1

            stateCountsPerEventType.put(eventType, newCount)

          } else {

            stateCountsPerEventType.put(eventType, 1)

          }

          //push output

          out.collect(s"${ctx.getCurrentKey} - ${stateCountsPerEventType.entries().asScala.mkString(",")}")

        }
      }
    )

    countsPerType.print()
    env.execute()


  }


  //2> User Diana - [events: 11] - [AddToShoppingCartEvent(Diana,a43ac384-34f8-4f6e-ad0f-09082f2590cf,8,2023-07-26T12:06:17.240335100Z),RemovedFromShoppingCartEvent(Diana,e8410f2b-48e3-4830-a6cb-5070a240c656,4,2023-07-26T12:06:18.240335100Z),AddToShoppingCartEvent(Diana,8134d98a-7808-4583-82f7-17bc9477b56d,1,2023-07-26T12:06:18.240335100Z),RemovedFromShoppingCartEvent(Diana,925a54fa-743c-4707-8af6-84577410585b,7,2023-07-26T12:06:19.240335100Z),RemovedFromShoppingCartEvent(Diana,fd3686a5-33d8-4315-9a03-8b31582af3e6,5,2023-07-26T12:06:19.240335100Z),RemovedFromShoppingCartEvent(Diana,dee4f92e-adcd-4ad9-baaf-687a539f307d,8,2023-07-26T12:06:19.240335100Z),AddToShoppingCartEvent(Diana,aef7abac-3549-4e7c-abc3-977b04ae3483,6,2023-07-26T12:06:19.240335100Z),AddToShoppingCartEvent(Diana,b50c0c8e-eef0-4074-981f-f3269219c546,0,2023-07-26T12:06:20.240335100Z),AddToShoppingCartEvent(Diana,b10713c4-8ff1-4bc7-86e2-2041cfa3d67e,1,2023-07-26T12:06:22.240335100Z),RemovedFromShoppingCartEvent(Diana,5d5ccfcf-3a07-48d4-9f65-cfee27f52597,3,2023-07-26T12:06:23.240335100Z),AddToShoppingCartEvent(Diana,844aadb2-fe5f-4aa5-9466-b87efe5e0403,2,2023-07-26T12:06:23.240335100Z)]
  //6> User Alice - [events: 1] - [RemovedFromShoppingCartEvent(Alice,8ea55601-c04e-440c-86d1-746a1064cf7c,2,2023-07-26T12:06:23.240335100Z)]
  //2> User Diana - [events: 1] - [AddToShoppingCartEvent(Diana,9bd60603-0a11-4a80-b58d-ca908c4c8b19,3,2023-07-26T12:06:23.240335100Z)]
  //6> User Alice - [events: 2] - [RemovedFromShoppingCartEvent(Alice,8ea55601-c04e-440c-86d1-746a1064cf7c,2,2023-07-26T12:06:23.240335100Z),AddToShoppingCartEvent(Alice,f14dbb85-bafa-4a91-98d3-5f33b364d32f,1,2023-07-26T12:06:24.240335100Z)]


  //Clear the State
  def demoListStateWithClearance(): Unit = {

    val eventsPerUser: KeyedStream[ShoppingCartEvent, String] = shoppingCartEvents.keyBy(_.userId)

    //store all the events by user id
    val allEventsPerUserStream = eventsPerUser.process(
      new KeyedProcessFunction[String, ShoppingCartEvent, String] {

        //if >10 elements -> clear the list
        var stateEventsForuser: ListState[ShoppingCartEvent] = _

        override def open(parameters: Configuration): Unit = {
          stateEventsForuser = getRuntimeContext.getListState(
            new ListStateDescriptor[ShoppingCartEvent]("shopping-cart-events", classOf[ShoppingCartEvent])
          )
        }


        override def processElement(value: ShoppingCartEvent,
                                    ctx: KeyedProcessFunction[String, ShoppingCartEvent, String]#Context,
                                    out: Collector[String]): Unit = {


          stateEventsForuser.add(value)

          val currentEvents = stateEventsForuser.get().asScala.toList

          if (currentEvents.size > 10) {
            stateEventsForuser.clear() //only a request -> not done immediately
          }

          out.collect(s"User ${value.userId} - [events: ${currentEvents.size}] - [${currentEvents.mkString(",")}]")

        }
      }
    )

    allEventsPerUserStream.print()
    env.execute()

  }

  def demoListStateWithTimeTriggeredClearance(): Unit = {

    val eventsPerUser: KeyedStream[ShoppingCartEvent, String] = shoppingCartEvents.keyBy(_.userId)

    //store all the events by user id
    val allEventsPerUserStream = eventsPerUser.process(
      new KeyedProcessFunction[String, ShoppingCartEvent, String] {

        //if >10 elements -> clear the list
        var stateEventsForuser: ListState[ShoppingCartEvent] = _

        override def open(parameters: Configuration): Unit = {

          val descriptor = new ListStateDescriptor[ShoppingCartEvent]("shopping-cart-events", classOf[ShoppingCartEvent])

          descriptor.enableTimeToLive(
            StateTtlConfig.newBuilder(Time.hours(1)) //clears the state after one hour
              .setUpdateType(StateTtlConfig.UpdateType.OnCreateAndWrite) //specify when the timer resets
              .setStateVisibility(StateTtlConfig.StateVisibility.ReturnExpiredIfNotCleanedUp)
              .build()
          )

          // time to live = cleared if its not modified after a certain time
          stateEventsForuser = getRuntimeContext.getListState(descriptor)

        }


        override def processElement(value: ShoppingCartEvent,
                                    ctx: KeyedProcessFunction[String, ShoppingCartEvent, String]#Context,
                                    out: Collector[String]): Unit = {


          stateEventsForuser.add(value)

          val currentEvents = stateEventsForuser.get().asScala.toList

          if (currentEvents.size > 10) {
            stateEventsForuser.clear() //only a request -> not done immediately
          }

          out.collect(s"User ${value.userId} - [events: ${currentEvents.size}] - [${currentEvents.mkString(",")}]")

        }
      }
    )

    allEventsPerUserStream.print()
    env.execute()

  }


  def main(args: Array[String]): Unit = {

    demoListStateWithClearance()

  }

}
