package part2datastreams

import org.apache.flink.api.common.functions.{FlatMapFunction, MapFunction, ReduceFunction}
import org.apache.flink.api.common.serialization.SimpleStringEncoder
import org.apache.flink.core.fs.Path
import org.apache.flink.streaming.api.functions.ProcessFunction
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink
import org.apache.flink.streaming.api.scala._
import org.apache.flink.util.Collector

object ExplicitTransformations {


  def demoExplicitTransformations(): Unit = {

    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    val numbers = env.fromSequence(1, 1000)


    val doubleNumbers = numbers.map(_ * 2)

    //explicit version
    val doubleNumbers_v2 = numbers.map(
      new MapFunction[Long, Long] {
        override def map(value: Long): Long = value * 2
      })

    val expandedNumbers = numbers.flatMap(n => Range.Long(1, n, 1).toList)

    //explicit version

    val expandedNumbers_v2 = numbers.flatMap(new FlatMapFunction[Long, Long] {
      override def flatMap(value: Long, out: Collector[Long]): Unit =
        Range.Long(1, value, 1).foreach({ i =>
          out.collect(i)
        })
    })


    // process method
    // Process Function is most general function to process elements in Flink
    val expandedNumbers_v3 = numbers.process(new ProcessFunction[Long, Long] {
      override def processElement(value: Long, ctx: ProcessFunction[Long, Long]#Context, out: Collector[Long]): Unit =
        Range.Long(1, value, 1).foreach({ i =>
          out.collect(i)
        })
    })



    // reduce - happends on keyed streams

    val keyedNumbers: KeyedStream[Long, Boolean] = numbers.keyBy(n => n % 2 == 0)

    //reduce - FP approach
    val sumByKey = keyedNumbers.reduce(_ + _) // sums up all the elements by key

    //reduce - explicit
    val sumByKey_v2 = keyedNumbers.reduce(new ReduceFunction[Long] {
      //additional fields methods etc.
      override def reduce(x: Long, y: Long): Long = x + y
    }).setParallelism(8)

    sumByKey_v2.print()

    sumByKey_v2.addSink(
      StreamingFileSink
        .forRowFormat(
          new Path("output/streaming_sink"),
          new SimpleStringEncoder[Long]("UTF-8")
        )
        .build()
    ).setParallelism(1)

    env.execute()
  }

  def main(args: Array[String]): Unit = {

    demoExplicitTransformations()

  }

}
