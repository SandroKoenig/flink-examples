package part2datastreams

import generators.gaming.{PlayerRegistered, ServerEvent, alice, bob, carl, mary, rob, sam}
import org.apache.flink.api.common.eventtime.{SerializableTimestampAssigner, WatermarkStrategy}
import org.apache.flink.api.common.functions.AggregateFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.{AllWindowFunction, ProcessAllWindowFunction, ProcessWindowFunction, WindowFunction}
import org.apache.flink.streaming.api.windowing.assigners.{EventTimeSessionWindows, GlobalWindows, SlidingEventTimeWindows, TumblingEventTimeWindows}
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.triggers.CountTrigger
import org.apache.flink.streaming.api.windowing.windows.{GlobalWindow, TimeWindow}
import org.apache.flink.util.Collector

import java.lang
import java.time.Instant
import scala.collection.convert.ImplicitConversions.`iterable AsScalaIterable`
import scala.concurrent.duration._

object WindowFunctions {

  //use-case: stream of events for a gaming session

  val env = StreamExecutionEnvironment.getExecutionEnvironment

  implicit val serverStartTime: Instant =
    Instant.parse("2022-02-02T00:00:00.000Z")

  val events: List[ServerEvent] = List(
    bob.register(2.seconds), //player "Bob" registered 2s after the server started
    bob.online(2.seconds),
    sam.register(3.seconds),
    sam.online(4.seconds),
    rob.register(4.seconds),
    alice.register(4.seconds),
    mary.register(6.seconds),
    mary.online(6.seconds),
    carl.register(8.seconds),
    rob.online(10.seconds),
    alice.online(10.seconds),
    carl.online(10.seconds)
  )


  //how many players were registered every 3 seconds
  //[0..3sec], [3...6sec], [6...9sec]

  val eventStream: DataStream[ServerEvent] = env
    .fromCollection(events)
    .assignTimestampsAndWatermarks( //extract timestamps for events (event time) + watermarks
      WatermarkStrategy
        .forBoundedOutOfOrderness(java.time.Duration.ofMillis(500)) //once you get an event with time T, yoi will not accept further events with time  < T - 500
        .withTimestampAssigner(new SerializableTimestampAssigner[ServerEvent] {
          override def extractTimestamp(element: ServerEvent, recordTimestamp: Long): Long = {
            element.eventTime.toEpochMilli
          }
        })
    )


  //count by windowAll
  val threeSecondsTumblingWindow = eventStream.windowAll(TumblingEventTimeWindows.of(Time.seconds(3)))

  class CountByWindowAll extends AllWindowFunction[ServerEvent, String, TimeWindow] {

    override def apply(window: TimeWindow, input: Iterable[ServerEvent], out: Collector[String]): Unit = {
      val registrationEventCount = input.count(event => event.isInstanceOf[PlayerRegistered])

      out.collect(s"Window [${window.getStart} - ${window.getEnd}] $registrationEventCount")
    }
  }

  //alternative:  context offers a much richer API
  class CountByWindowAllV2 extends ProcessAllWindowFunction[ServerEvent, String, TimeWindow] {

    override def process(context: Context, elements: Iterable[ServerEvent], out: Collector[String]): Unit = {

      val window = context.window

      val registrationEventCount = elements.count(event => event.isInstanceOf[PlayerRegistered])

      out.collect(s"Window [${window.getStart} - ${window.getEnd}] $registrationEventCount")


    }
  }

  //aggregate functions
  class CountByWindowV3 extends AggregateFunction[ServerEvent, Long, Long] {
    override def createAccumulator(): Long = 0L

    //every element increaes accumulator by 1
    override def add(value: ServerEvent, accumulator: Long): Long = {
      if (value.isInstanceOf[PlayerRegistered]) accumulator + 1
      else accumulator
    }

    //push final output out of the final accumulator
    override def getResult(accumulator: Long): Long = accumulator

    // acc1 + acc2 = biggerAcc
    override def merge(a: Long, b: Long): Long = a + b
  }


  def demoCountByWindow(): Unit = {

    val registrationsPerThreeSeconds: DataStream[String] = threeSecondsTumblingWindow.apply(new CountByWindowAll)

    registrationsPerThreeSeconds.print()

    env.execute()

  }

  def demoCountByWindowV2(): Unit = {

    val registrationsPerThreeSeconds: DataStream[String] = threeSecondsTumblingWindow.process(new CountByWindowAllV2)

    registrationsPerThreeSeconds.print()

    env.execute()

  }

  def demoCountByWindowV3(): Unit = {

    val registrationsPerThreeSeconds: DataStream[Long] = threeSecondsTumblingWindow.aggregate(new CountByWindowV3)

    registrationsPerThreeSeconds.print()

    env.execute()

  }


  /**
   * Keyed Streams and window functions
   */

  //each element will be assigend to a "mini-stream" for its own key
  val streamByType: KeyedStream[ServerEvent, String] = eventStream.keyBy(e => e.getClass.getSimpleName)


  //for every key we will have a separate window allocation
  val threeSecondsTumblingWindowsByType = streamByType.window(TumblingEventTimeWindows.of(Time.seconds(3)))


  class CountInWindow extends WindowFunction[ServerEvent, String, String, TimeWindow] {

    override def apply(key: String, window: TimeWindow, input: Iterable[ServerEvent], out: Collector[String]): Unit = {

      out.collect(s"$key: $window, ${input.size}")

    }
  }

  def countByTypeByWindow(): Unit = {

    val finalStream = threeSecondsTumblingWindowsByType.apply(new CountInWindow)

    finalStream.print()

    env.execute()


  }

  class CountInWindowV2 extends ProcessWindowFunction[ServerEvent, String, String, TimeWindow] {

    override def process(key: String, context: Context, elements: Iterable[ServerEvent], out: Collector[String]): Unit = {

      out.collect(s"$key: ${context.window}, ${elements.size}")

    }
  }

  def countByTypeByWindowV2(): Unit = {

    val finalStream = threeSecondsTumblingWindowsByType.process(new CountInWindowV2)

    finalStream.print()

    env.execute()


  }

  //one task processes all the data for a particular key

  /**
   * Sliding Windows - Windows that can overlap
   */


  //how many players were registered every 3 seconds - UPDATED EVERY 1 SEC
  //[0..3sec], [1...4sec], [2...5sec]

  def SlidingAllWindows(): Unit = {

    val windowSize: Time = Time.seconds(3)
    val slidingTime: Time = Time.seconds(1)

    val slidingWindowsAll = eventStream.windowAll(SlidingEventTimeWindows.of(windowSize, slidingTime))

    // process the window stream with similar window functions

    val registrationCountByWindow = slidingWindowsAll.apply(new CountByWindowAll)

    registrationCountByWindow.print()

    env.execute()
  }

  /**
   * Session Windows = Group of events with no more than a certain time gap in between them
   */

  // How many registrations events do we have no more than 1 SEC apart?

  def demoSessionWindows(): Unit = {

    val groupBySessionWindows = eventStream.windowAll(EventTimeSessionWindows.withGap(Time.seconds(1)))

    //operate any kind of window fucntion

    val countBySessionWindows = groupBySessionWindows.apply(new CountByWindowAll)

    countBySessionWindows.print()

    env.execute()

  }

  /**
   * Global Windows
   */

  //HOW MANY REGISTRATION EVENTS DO WE HAVE EVERY 10 EVENTS?

  class CountByGlobalWindowAll extends AllWindowFunction[ServerEvent, String, GlobalWindow] {

    override def apply(window: GlobalWindow, input: Iterable[ServerEvent], out: Collector[String]): Unit = {
      val registrationEventCount = input.count(event => event.isInstanceOf[PlayerRegistered])

      out.collect(s"Window [${window}] $registrationEventCount")
    }
  }


  def demoGlobalWindow(): Unit = {

    val globalWindowEvents = eventStream.windowAll(GlobalWindows.create())
      .trigger(CountTrigger.of[GlobalWindow](10))
      .apply(new CountByGlobalWindowAll())

    globalWindowEvents.print()

    env.execute()

  }

  /**
   * EXERCISE: What was the time windows (continous 2sec) when we had the most/highest number of registration events?
   */


  class MostRegistrationsByWindow extends AllWindowFunction[ServerEvent, (TimeWindow, Int), TimeWindow] {

    override def apply(window: TimeWindow, input: Iterable[ServerEvent], out: Collector[(TimeWindow, Int)]): Unit = {
      val registrationEventCount = input.count(event => event.isInstanceOf[PlayerRegistered])

      out.collect(new Tuple2[TimeWindow, Int](window, registrationEventCount))
    }
  }


  def exerciseMostRegistrationsByWindow(): Unit = {

    val windowSize: Time = Time.seconds(2)
    val slidingTime: Time = Time.seconds(1)

    val mostRegistreationsByWindow = eventStream.windowAll(SlidingEventTimeWindows.of(windowSize, slidingTime))
      .apply(new MostRegistrationsByWindow)
      .executeAndCollect()
      .toList
      .maxBy(_._2)

    println(s"The Window: ${mostRegistreationsByWindow._1.getStart} to ${mostRegistreationsByWindow._1.getEnd} " +
      s"has the most registrations with a total of ${mostRegistreationsByWindow._2}")

  }

  def main(args: Array[String]): Unit = {

    exerciseMostRegistrationsByWindow()
  }

}
