package part2datastreams

import generators.shopping._
import org.apache.flink.api.common.eventtime.{SerializableTimestampAssigner, WatermarkStrategy}
import org.apache.flink.streaming.api.functions.co.{CoProcessFunction, ProcessJoinFunction}
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.util.Collector

object MultipleStreams {

  val env = StreamExecutionEnvironment.getExecutionEnvironment

  /**
   * Techniques
   *  - Union
   *  - Window Join
   *  - Interval Join
   *  - Connect
   */


  // UNIONING = Combining the output of multiple streams into just one

  //  5> AddToShoppingCartEvent(Bob,files_965e16d3-9eb4-4531-8211-83e5dbf3cc93,5,2023-07-25T09:11:31.494392400Z)
  //  6> AddToShoppingCartEvent(Sam,kafka_1ff9eeaf-a78a-4918-bda7-1b1668f3604a,4,2023-07-25T09:11:31.146393400Z)
  //  7> AddToShoppingCartEvent(Sam,kafka_50495df4-2cea-489b-b221-7de0274ec859,2,2023-07-25T09:11:31.146393400Z)
  //  6> AddToShoppingCartEvent(Diana,files_b5eb4005-8297-4c5e-9c1f-337183b67669,7,2023-07-25T09:11:31.494392400Z)
  //  5> AddToShoppingCartEvent(Diana,kafka_d1a37956-4442-44fe-a933-5b2a460cf614,0,2023-07-25T09:11:31.146393400Z)
  //  7> AddToShoppingCartEvent(Sam,files_9cdc118b-6a60-41e5-917b-0440d1e65dbb,0,2023-07-25T09:11:31.494392400Z)
  //  8> AddToShoppingCartEvent(Alice,kafka_0b9568c1-7d56-435c-93a5-5e6c73c558f5,3,2023-07-25T09:11:31.146393400Z)
  //  8> AddToShoppingCartEvent(Sam,files_6c80d0fd-dee8-4140-9823-27a2348551a8,1,2023-07-25T09:11:31.494392400Z)
  //  2> AddToShoppingCartEvent(Tom,files_0b4f7093-8b00-4c32-8e1c-c3872c14b111,6,2023-07-25T09:11:31.494392400Z)
  //  1> AddToShoppingCartEvent(Bob,files_4b16b3f9-bcf1-47b1-a8bb-e1da1add0285,1,2023-07-25T09:11:31.494392400Z)
  //  3> AddToShoppingCartEvent(Tom,kafka_52077dc1-c38f-47fc-945b-d1a392f26056,4,2023-07-25T09:11:31.146393400Z)


  def demoUnion(): Unit = {

    val env1 = StreamExecutionEnvironment.getExecutionEnvironment

    //define two streams of the same type
    val shoppingCartEventsKafka: DataStream[ShoppingCartEvent] = env1
      .addSource(new SingleShoppingCartEventsGenerator(300, sourceId = Option("kafka")))


    val shoppingCartEventsFiles: DataStream[ShoppingCartEvent] = env1
      .addSource(new SingleShoppingCartEventsGenerator(100, sourceId = Option("files")))

    val combinedShoppingCartEventStream: DataStream[ShoppingCartEvent] =
      shoppingCartEventsKafka.union(shoppingCartEventsFiles)

    combinedShoppingCartEventStream.print()

    env1.execute()

  }

  // WINDOW JOIN -> Elements belong to the same window + some join condition

  //    1> USER Sam browsed at 2023-07-25T09:22:43.289270900Z and bought at 2023-07-25T09:22:42.937270700Z
  //    2> USER Diana browsed at 2023-07-25T09:22:43.289270900Z and bought at 2023-07-25T09:22:42.937270700Z
  //    6> USER Alice browsed at 2023-07-25T09:22:43.289270900Z and bought at 2023-07-25T09:22:43.937270700Z
  //    8> USER Bob browsed at 2023-07-25T09:22:43.289270900Z and bought at 2023-07-25T09:22:45.937270700Z
  //    6> USER Alice browsed at 2023-07-25T09:22:43.289270900Z and bought at 2023-07-25T09:22:43.937270700Z
  //    1> USER Sam browsed at 2023-07-25T09:22:43.289270900Z and bought at 2023-07-25T09:22:42.937270700Z
  //    2> USER Diana browsed at 2023-07-25T09:22:44.289270900Z and bought at 2023-07-25T09:22:42.937270700Z
  //    1> USER Sam browsed at 2023-07-25T09:22:44.289270900Z and bought at 2023-07-25T09:22:42.937270700Z

  def demoWindowJoins(): Unit = {

    val env1 = StreamExecutionEnvironment.getExecutionEnvironment

    val shoppingCartEventsKafka: DataStream[ShoppingCartEvent] = env1
      .addSource(new SingleShoppingCartEventsGenerator(1000, sourceId = Option("kafka")))

    val catalogueEvents: DataStream[CatalogEvent] = env1
      .addSource(new CatalogEventsGenerator(200))


    val joinedStream = shoppingCartEventsKafka
      .join(catalogueEvents)
      //providing a join condition
      .where(shoppingCartEvent => shoppingCartEvent.userId)
      .equalTo(catalogueEvent => catalogueEvent.userId)
      //provide the same window grouping
      .window(TumblingProcessingTimeWindows.of(Time.seconds(5)))
      // doe something with the correlated events
      .apply(
        (shoppingCartEvent, catalogueEvent) =>
          s"USER ${shoppingCartEvent.userId} browsed at ${catalogueEvent.time} and bought at ${shoppingCartEvent.time}"
      )


    joinedStream.print()

    env1.execute()

  }


  // INTERVAL JOINS = Correlation between Events A and B if durationMin < timeA - timeB < durationMax
  // involves EventTime
  // Only works on KEYED STREAMS

  //    6> USER Alice browsed at 2023-07-25T09:35:28.586616700Z and bought  at 2023-07-25T09:35:28.156616300Z
  //    1> USER Sam browsed at 2023-07-25T09:35:28.586616700Z and bought  at 2023-07-25T09:35:28.156616300Z
  //    2> USER Diana browsed at 2023-07-25T09:35:28.586616700Z and bought  at 2023-07-25T09:35:28.156616300Z
  //    1> USER Sam browsed at 2023-07-25T09:35:28.586616700Z and bought  at 2023-07-25T09:35:28.156616300Z
  //    8> USER Tom browsed at 2023-07-25T09:35:28.586616700Z and bought  at 2023-07-25T09:35:28.156616300Z
  //    2> USER Diana browsed at 2023-07-25T09:35:28.586616700Z and bought  at 2023-07-25T09:35:28.156616300Z
  //    8> USER Tom browsed at 2023-07-25T09:35:28.586616700Z and bought  at 2023-07-25T09:35:28.156616300Z

  def demoIntervalJoins(): Unit = {

    val env1 = StreamExecutionEnvironment.getExecutionEnvironment

    // We need to extract event times from both streams

    val shoppingCartEventsKafka = env1
      .addSource(new SingleShoppingCartEventsGenerator(300, sourceId = Option("kafka")))
      .assignTimestampsAndWatermarks(
        WatermarkStrategy.forBoundedOutOfOrderness(java.time.Duration.ofMillis(500))
          .withTimestampAssigner(new SerializableTimestampAssigner[ShoppingCartEvent] {
            override def extractTimestamp(element: ShoppingCartEvent, recordTimestamp: Long): Long = {

              element.time.toEpochMilli
            }
          })
      )
      .keyBy(_.userId)

    val catalogueEvents = env1
      .addSource(new CatalogEventsGenerator(500))
      .assignTimestampsAndWatermarks(
        WatermarkStrategy.forBoundedOutOfOrderness(java.time.Duration.ofMillis(500))
          .withTimestampAssigner(new SerializableTimestampAssigner[CatalogEvent] {
            override def extractTimestamp(element: CatalogEvent, recordTimestamp: Long): Long = {

              element.time.toEpochMilli
            }
          })
      )
      .keyBy(_.userId)

    val intervallJoinedStreams = shoppingCartEventsKafka
      .intervalJoin(catalogueEvents)
      .between(Time.seconds(-2), Time.seconds(2))
      .lowerBoundExclusive() // Interval is by default inclusive
      .upperBoundExclusive()
      .process(new ProcessJoinFunction[ShoppingCartEvent, CatalogEvent, String] {

        override def processElement(left: ShoppingCartEvent,
                                    right: CatalogEvent,
                                    ctx: ProcessJoinFunction[ShoppingCartEvent, CatalogEvent, String]#Context,
                                    out: Collector[String]): Unit = {

          out.collect(s"USER ${left.userId} browsed at ${right.time} and bought  at ${left.time}")

        }
      })


    intervallJoinedStreams.print()

    env1.execute()

  }

  // CONNECT = Two Streams are treated with the same "operator"

  def demoConnect(): Unit = {

    val env1 = StreamExecutionEnvironment.getExecutionEnvironment

    env1.setParallelism(1)
    env1.setMaxParallelism(1)


    //two seperate streams

    val shoppingCartEventsKafka: DataStream[ShoppingCartEvent] = env1
      .addSource(new SingleShoppingCartEventsGenerator(100))
      .setParallelism(1)

    val catalogueEvents: DataStream[CatalogEvent] = env1
      .addSource(new CatalogEventsGenerator(1000))
      .setParallelism(1)

    //CONNECT

    val connectedStream: ConnectedStreams[ShoppingCartEvent, CatalogEvent] = shoppingCartEventsKafka
      .connect(catalogueEvents)

    //variables - will use single threaded

    val ratioStream: DataStream[Double] = connectedStream.process(
      //Processing a to different Events a the same time
      new CoProcessFunction[ShoppingCartEvent, CatalogEvent, Double] {

        var shoppingCartEventCount = 0;
        var catalogueEventCount = 0;

        override def processElement1(value: ShoppingCartEvent,
                                     ctx: CoProcessFunction[ShoppingCartEvent, CatalogEvent, Double]#Context,
                                     out: Collector[Double]): Unit = {


          shoppingCartEventCount += 1;
          out.collect(shoppingCartEventCount * 100.0 / (shoppingCartEventCount + catalogueEventCount))

        }

        override def processElement2(value: CatalogEvent,
                                     ctx: CoProcessFunction[ShoppingCartEvent, CatalogEvent, Double]#Context,
                                     out: Collector[Double]): Unit = {

          catalogueEventCount += 1;
          out.collect(shoppingCartEventCount * 100.0 / (shoppingCartEventCount + catalogueEventCount))

        }
      }
    )

    ratioStream.print()
    env1.execute()

  }


  def main(args: Array[String]): Unit = {

    demoConnect()

  }


}
