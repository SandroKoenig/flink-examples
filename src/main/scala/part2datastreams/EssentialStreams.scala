package part2datastreams

import org.apache.flink.streaming.api.scala._

import scala.collection.immutable.Nil.:::
import scala.util.Random // import TypeInformation for the data of your DataStreams

object EssentialStreams {


  def applicationTemplate(): Unit = {

    // 1) Execution Environment
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment


    //in between, add any sort of computations

    val simpleNumberStream: DataStream[Int] = env.fromElements(1, 2, 3, 4)


    //perform some actions
    simpleNumberStream.print()


    //at the end
    env.execute() //trigger all the computations that were described earlier

  }


  // transformations

  def demoTransformations(): Unit = {
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    //checking parallelism
    println(s"Current Parallelism: ${env.getParallelism}")

    val random = new Random()
    val randnumbers = Seq.fill(1000000)(random.nextInt(1000))

    val numbers: DataStream[Int] = env.fromCollection(randnumbers)

    val doubleNumbers: DataStream[Int] = numbers.map(_ * 2)

    val expandedNumbers: DataStream[Int] = numbers.flatMap(n => List(n, n + 1))

    val filteredNumbers: DataStream[Int] = numbers.filter(_ % 2 == 0).setParallelism(3) /* Parallelism cna be adjusted at every step of the processing */


    expandedNumbers.writeAsText("output/expandedStream.txt").setParallelism(1)

    env.execute()
  }

  def main(args: Array[String]): Unit = {

    demoTransformations()
  }

}
