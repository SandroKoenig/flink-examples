package part2datastreams

import org.apache.flink.api.common.serialization.SimpleStringEncoder
import org.apache.flink.core.fs.Path
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink
import org.apache.flink.streaming.api.scala._


object FizzBuzz {


  def fizzBuzz(): Unit = {

    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    val oneToAHundred: DataStream[Int] = env.fromCollection(1 to 100 toArray)

    val onlyFizzBuzz = oneToAHundred
      .map(number => fizzOrBuzz(number))
      .filter(tuple2 => tuple2._2.equals("fizzbuzz"))
      .map(_._1)

    onlyFizzBuzz.writeAsText("output/fizzbuzz.txt").setParallelism(1)

    env.execute()
  }


  def fizzBuzzWithSink(): Unit = {

    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    val oneToAHundred = env.fromSequence(1, 100)

    val onlyFizzBuzz = oneToAHundred
      .map(number => fizzOrBuzz(number))
      .filter(tuple2 => tuple2._2.equals("fizzbuzz"))
      .map(_._1)


    //add sink
    onlyFizzBuzz.addSink(
      StreamingFileSink
        .forRowFormat(
          new Path("output/streaming_sink"),
          new SimpleStringEncoder[Long]("UTF-8")
        )
        .build()
    )


    env.execute()
  }


  def fizzOrBuzz(number: Long): (Long, String) = {

    number match {
      case x if (x % 3 == 0) && (x % 5 == 0) => (x, "fizzbuzz")
      case x if (x % 3 == 0) => (x, "fizz")
      case x if (x % 5 == 0) => (x, "buzz")
      case x => (x, "")
    }

  }


  def main(args: Array[String]): Unit = {

    fizzBuzzWithSink()

  }


}
