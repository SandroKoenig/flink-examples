package part2datastreams

import generators.shopping._
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.ProcessAllWindowFunction
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.triggers.{CountTrigger, PurgingTrigger}
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

object Triggers {


  //Triggers --> WHEN A WINDOW FUNCTION IS EXECUTED

  val env = StreamExecutionEnvironment.getExecutionEnvironment

  def demoCountTrigger(): Unit = {

    val shoppingCartEvents: DataStream[String] = env
      .addSource(new ShoppingCartEventsGenerator(500, 2)) // 2 events/second
      .windowAll(TumblingProcessingTimeWindows.of(Time.seconds(5)))
      .trigger(CountTrigger.of[TimeWindow](5)) // Window function is triggered to run every 5 elements
      .process(new CountByWindowAll) //runs twice for the same window

    shoppingCartEvents.print()
    env.execute()

  }

  //  4> Window [1690273570000 - 1690273575000] 2
  //  5> Window [1690273575000 - 1690273580000] 10
  //  6> Window [1690273580000 - 1690273585000] 10
  //  7> Window [1690273585000 - 1690273590000] 10
  //  8> Window [1690273590000 - 1690273595000] 10
  //  1> Window [1690273595000 - 1690273600000] 10
  //  2> Window [1690273600000 - 1690273605000] 10
  //  3> Window [1690273605000 - 1690273610000] 10
  //  4> Window [1690273610000 - 1690273615000] 10
  //  5> Window [1690273615000 - 1690273620000] 10
  //  6> Window [1690273620000 - 1690273625000] 10
  //  7> Window [1690273625000 - 1690273630000] 10
  //  8> Window [1690273630000 - 1690273635000] 10
  //  1> Window [1690273635000 - 1690273640000] 10
  //  2> Window [1690273640000 - 1690273645000] 10
  //  3> Window [1690273645000 - 1690273650000] 10


  //WITH TRIGGER

  //  1> Window [1690273770000 - 1690273775000] 5 <-- TRIGGER running on the window 70000-75000 for the first time
  //  2> Window [1690273770000 - 1690273775000] 10<-- SECOND TRIGGER for the same window
  //  3> Window [1690273775000 - 1690273780000] 5
  //  4> Window [1690273775000 - 1690273780000] 10
  //  5> Window [1690273780000 - 1690273785000] 5
  //  6> Window [1690273780000 - 1690273785000] 10
  //  7> Window [1690273785000 - 1690273790000] 5


  // PURGING TRIGGER - clears the window when it fires

  //  2> Window [1690274090000 - 1690274095000] 5
  //  3> Window [1690274095000 - 1690274100000] 5
  //  4> Window [1690274095000 - 1690274100000] 5
  //  5> Window [1690274100000 - 1690274105000] 5
  //  6> Window [1690274100000 - 1690274105000] 5
  //  7> Window [1690274105000 - 1690274110000] 5
  //  8> Window [1690274105000 - 1690274110000] 5

  def demoPurgingTrigger(): Unit = {

    val shoppingCartEvents: DataStream[String] = env
      .addSource(new ShoppingCartEventsGenerator(500, 2)) // 2 events/second
      .windowAll(TumblingProcessingTimeWindows.of(Time.seconds(5)))
      .trigger(PurgingTrigger.of(CountTrigger.of[TimeWindow](5))) // Window function is triggered to run every 5 elements, THEN CLEARS THE WINDOW
      .process(new CountByWindowAll) //runs twice for the same window

    shoppingCartEvents.print()
    env.execute()

  }

  /**
   * OTHER TRIGGERS
   *  - EventTimeTrigger -> happens by default when watermark is > window end time (automatic for event time windows)
   *  - ProcessingTimeTrigger -> fires when the current system time > window end time (automatic for processing windows)
   *  - CustomTrigger -> Powerful APIs for custom firing behaviour
   */


  class CountByWindowAll extends ProcessAllWindowFunction[ShoppingCartEvent, String, TimeWindow] {

    override def process(context: Context, elements: Iterable[ShoppingCartEvent], out: Collector[String]): Unit = {

      val window = context.window
      out.collect(s"Window [${window.getStart} - ${window.getEnd}] ${elements.size}")

    }
  }


  def main(args: Array[String]): Unit = {

    demoPurgingTrigger()

  }


}
