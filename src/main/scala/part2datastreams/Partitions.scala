package part2datastreams

import generators.shopping.{ShoppingCartEvent, SingleShoppingCartEventsGenerator}
import org.apache.flink.api.common.functions.Partitioner
import org.apache.flink.streaming.api.scala._

object Partitions {

  //Splitting = Partitioning

  //  Number of max partitions: 8
  //  Number of max partitions: 8
  //  Number of max partitions: 8
  //  Number of max partitions: 8
  //  Number of max partitions: 8
  //  Number of max partitions: 8
  //  Number of max partitions: 8
  //  Number of max partitions: 8
  //  Number of max partitions: 8
  //  1> AddToShoppingCartEvent(Alice,29414d79-0265-4564-9ef8-5fd571d18aa3,5,2023-07-25T15:00:49.256239Z)
  //  8> AddToShoppingCartEvent(Diana,16cb106a-b64c-488e-b496-39e5f1d3955c,9,2023-07-25T15:00:49.256239Z)
  //  3> AddToShoppingCartEvent(Tom,541f66d9-8c24-4ae8-b4c6-85867bd4e02c,0,2023-07-25T15:00:49.256239Z)
  //  1> AddToShoppingCartEvent(Alice,5dc07205-c8f2-4615-b7d5-e837b2e37e96,8,2023-07-25T15:00:50.256239Z)
  //  3> AddToShoppingCartEvent(Tom,6ca59bd2-8381-4974-86bc-e494d3209e20,9,2023-07-25T15:00:49.256239Z)
  //  8> AddToShoppingCartEvent(Sam,7a399267-b493-4fee-9601-27ffd1000733,5,2023-07-25T15:00:49.256239Z)
  //  3> AddToShoppingCartEvent(Tom,5a578ea4-4549-409d-85de-914a0b96eb4a,0,2023-07-25T15:00:50.256239Z)
  //  1> AddToShoppingCartEvent(Alice,25f1ddab-5de0-4e64-92ae-3695b0e3d404,4,2023-07-25T15:00:49.256239Z)

  def demoPartitioner(): Unit = {

    val env = StreamExecutionEnvironment.getExecutionEnvironment

    val shoppingCartEvents: DataStream[ShoppingCartEvent] = env
      .addSource(new SingleShoppingCartEventsGenerator(100)) // 10 Events/Second

    // Bad because
    // - you loose parallelism
    // - you risk overloading the task with disproportionate data
    //
    // Good for e.g. HTTP Requests
    val badPartitioner = new Partitioner[String] {
      override def partition(key: String, numPartitions: Int): Int = {
        numPartitions - 1
      }
    }


    //.shuffle -> redristibution of data evenly
    // Involves Network Calls

    // Logic to split the data
    val partitioner = new Partitioner[String] {
      //Invoked on every event
      override def partition(key: String, numPartitions: Int): Int = {
        //hashcode mod no of partitions
        println(s"Number of max partitions: $numPartitions")
        key.hashCode % numPartitions
      }
    }

    val partitionedStream = shoppingCartEvents.partitionCustom(
      partitioner,
      event => event.userId
    )


    val badPartitionedStream = shoppingCartEvents.partitionCustom(
      badPartitioner,
      event => event.userId
    )


    partitionedStream.print()

    env.execute()

  }


  def main(args: Array[String]): Unit = {

    demoPartitioner()

  }


}
