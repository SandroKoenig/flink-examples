package part4io

import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.source.{RichSourceFunction, SourceFunction}
import org.apache.flink.streaming.api.scala._

import java.io.PrintStream
import java.net.{ServerSocket, Socket}
import scala.util.Random

object CustomSources {


  //Source of numbers randomly generated
  class RandomNumberGeneratorSource(minEventsPerSeconds: Double)
    extends SourceFunction[Long] {

    //create local fields and methods
    val maxSleepTime: Long = (1000 / minEventsPerSeconds).toLong
    var isRunning: Boolean = true

    //Only called once
    //runs on a dedicated thread
    override def run(ctx: SourceFunction.SourceContext[Long]): Unit = {

      while (isRunning) {
        val sleepTime = Math.abs(Random.nextLong() % maxSleepTime)
        val nextNumber = Random.nextLong()

        Thread.sleep(sleepTime)

        //push sth to the output
        ctx.collect(nextNumber)
      }
    }


    //Called at application shutdown
    //contract: run method should stop immediately
    override def cancel(): Unit = {
      isRunning = false
    }
  }


  def demoSourceFunction(): Unit = {

    val env = StreamExecutionEnvironment.getExecutionEnvironment

    val numberStream = env.addSource(new RandomNumberGeneratorSource(10))

    numberStream.print()
    env.execute()

  }

  class SocketStringSource(host: String, port: Int) extends RichSourceFunction[String] {

    //resource needs to be opened and close -> whenever managing resources use rich functions
    var socket: Socket = _
    var isRunning: Boolean = true

    override def run(ctx: SourceFunction.SourceContext[String]): Unit = ???

    override def cancel(): Unit = ???

    override def open(parameters: Configuration): Unit = {
      socket = new Socket(host, port)
    }
  }


  object DataSender {

    def main(args: Array[String]): Unit = {

      val serverSocket = new ServerSocket(12345)

      print("Waiting for Flink to connect...")

      val socket = serverSocket.accept()

      print("Flink to connected")

      val printer = new PrintStream(socket.getOutputStream)

      printer.println("Hello from the other side..")

      Thread.sleep(3000)

      printer.println("Almost ready..")

      Thread.sleep(500)

      (1 to 10).foreach { i =>
        Thread.sleep(200)
        printer.println(s"Number $i")
      }

      println("Data Sending Complete")
      serverSocket.close()

    }

  }


  def main(args: Array[String]): Unit = {

    demoSourceFunction()

  }


}
